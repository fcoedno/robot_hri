cmake_minimum_required(VERSION 2.8.3)

project(robot_hri)

find_package(catkin REQUIRED COMPONENTS
  cv_bridge
  image_transport
  roscpp
  sound_play
)

catkin_package()

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
# include_directories(include)
include_directories(
  ${catkin_INCLUDE_DIRS}
  ./include/robot_hri/
  opencv_core
  opencv_highgui
  opencv_imgproc
  opencv_objdetect
)

## Declare a cpp library
# add_library(robot_hri
#   src/${PROJECT_NAME}/robot_hri.cpp
# )

## Declare a cpp executable
 add_executable(robot_hri_node src/robot_hri_node.cpp)

## Add cmake target dependencies of the executable/library
## as an example, message headers may need to be generated before nodes
# add_dependencies(robot_hri_node robot_hri_generate_messages_cpp)

## Specify libraries to link a library or executable target against
 target_link_libraries(robot_hri_node
   ${catkin_LIBRARIES}
 ) 
