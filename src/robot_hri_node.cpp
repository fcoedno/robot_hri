#include "ros/ros.h"
#include "FaceDetect.h"

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "facedetect");
    FaceDetect facedetect; 
    ros::spin();

    return 0;
}
