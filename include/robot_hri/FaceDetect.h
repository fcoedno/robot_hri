#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "ros/ros.h"

#include "sound_play/sound_play.h"
#include "image_transport/image_transport.h"
#include "sensor_msgs/image_encodings.h"
#include "cv_bridge/cv_bridge.h"

class FaceDetect
{
    public:
        FaceDetect()
         :it_(nh_),
         nh_private_("~")
        {
            image_sub_ = it_.subscribe("/camera/rgb/image_raw", 1, &FaceDetect::imageCallBack, this); //subscritor de rgb
            face_pub_ = it_.advertise("/detected_faces", 1);
            nh_private_.param<std::string>("classifier_path", classifier_path_, "");

            ROS_INFO("Caminho para o classificador: %s", classifier_path_.c_str());

            if(classifier_.load(classifier_path_)) ROS_INFO("Classificador carregado");
            else ROS_ERROR("Classificador não encontrado");
        }

        ~FaceDetect()
        {
        }

        void imageCallBack(const sensor_msgs::ImageConstPtr& image)
        {
            try
            {
                cv_ptr_ = cv_bridge::toCvCopy(image, sensor_msgs::image_encodings::BGR8);
            }
            catch(cv_bridge::Exception &e)
            {
                ROS_ERROR("cv_bridge Exception error: %s", e.what());
                return;
            }

            detectAndSay();
        }

    private:
        //NodeHandles
        ros::NodeHandle nh_;
        ros::NodeHandle nh_private_;

        //Imagetransport
        image_transport::ImageTransport it_;
        image_transport::Subscriber image_sub_;
        image_transport::Publisher face_pub_;

        //SoundPlay para executar sons
        sound_play::SoundClient sc_;

        //cv_ptr para imagens recebidas
        cv_bridge::CvImagePtr cv_ptr_;

        //Classificador de faces
        cv::CascadeClassifier classifier_;
        std::string classifier_path_; //caminho para o classifier, tem que ser configurado por parâmetros

        void detectAndSay()
        {
            cv::Mat gray;
            cv::vector<cv::Rect> faces;

            cv::cvtColor(cv_ptr_->image, gray, cv::COLOR_BGR2GRAY);
            cv::equalizeHist(gray, gray);

            classifier_.detectMultiScale( gray, faces, 1.1, 2, 0|cv::CASCADE_SCALE_IMAGE, cv::Size(30, 30) );
            int numFaces = faces.size();

            for(int i = 0; i<numFaces; i++)
            {
                cv::Point center( faces[i].x + faces[i].width/2, faces[i].y + faces[i].height/2 );
                ellipse( cv_ptr_->image, center, cv::Size( faces[i].width/2, faces[i].height/2), 0, 0, 360, cv::Scalar( 255, 0, 255 ), 4, 8, 0 );

                face_pub_.publish(cv_ptr_->toImageMsg());
            }

            if(numFaces)
            {
                sc_.say("Hello Human, what is your name?");
                ros::Duration(3).sleep();
                sc_.say("My name is Robot, nice to meet you");
                ros::Duration(5).sleep();
            }
        } 
};
